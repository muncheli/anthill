# Anthill library

developed as part of spring 2020 CMSE802

Code has been linted using pylint and autopep8:

```bash
pylint anthill
```

Documentaiton has been generated using:

```bash
pdoc --force --html --output-dir ./docs anthill
```

Tests can be run using:

```bash
pytest 
```
